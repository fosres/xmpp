#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include <openssl/sha.h>

// consider whether or not you must change
// the dimensions to [layer][tree]

#if 0
Replace all standard OpenSSL functions

with LibreSSL.
#endif

#if 0
Fix Algorithm 9 so that the output of

ltree is filled directly in ltree()

and not returned.

#endif

#if 0
1. Turns out the SK struct should have

to store 2^h WOTS+ private keys, where 

each private key itself is a len array

of n-byte strings. That takes three pointers

to store!

You need to fix all algorithms that must

reference the private keys in the SK

struct for this reason.

And that means Algorithms 9-11 have to be

reviewed in light of this insight.
#endif

#if 0
When returning arrays, be sure to

initialize them in the heap or else

the array will be deleted.
#endif

#if 0
See Section 2.5 on "Hash Function

Address Scheme"
#endif

#define ltree_address ots_address
#define tree_height chain_address
#define tree_index hash_address

typedef struct adrs
{
	uint32_t layer_address;

	uint32_t tree_address;

	uint32_t type;

// ots_address is at same position as

// l-tree address

	uint32_t ots_address; 

// chain_address is at same position as

// tree height

	uint32_t chain_address;

// hash address is at same position as

// tree index

	uint32_t hash_address;


	uint32_t key_and_mask;
} adrs;

typedef struct SK
{
	unsigned char *** wots_sk; // [(size_t an 2^h x len x n array: a 3D array
	unsigned char * sk_prf; // a n-byte array
	
	unsigned char * root; // an n-byte array
	
	unsigned char * seed; // an n-byte array

	size_t idx;

	size_t h;

} SK;



typedef struct SK_MT
{
	SK ** sk_map; // [( a d (layer ) x h (height)  2D array of pointers to SK structs)]
				   
	unsigned char * sk_prf; // a n-byte array
	
	unsigned char * root; // an n-byte array
	
	unsigned char * seed; // an n-byte array

	size_t idx_mt;

	size_t h;

	size_t d;
			
} SK_MT;

typedef struct PK
{
	unsigned char * root_node; // an n-byte array

	unsigned char * seed; // an n-byte array

	size_t algorithm_len = 3;

	unsigned char * algorithm; // a fixed string literal specifying algorith type

} PK;

typedef struct PK_MT
{
	unsigned char * root;

	unsigned char * seed;

	unsigned char * algorithm;
} PK_MT;

typedef struct Sig
{	
	unsigned char ** sig_ots; // an [len][n] array

	unsigned char ** auth; // an [h][n] array	
	
	unsigned char * r; // an n-byte array
	
	size_t idx_sig;

} Sig;

typedef struct Sig_MT
{
	size_t idx_sig;

	unsigned char * r; // an n-byte random string

	Sig * sig_arr; // an array of pointers to Sig structs
			//
			// each struct is of size_t (h / d + len ) * n bytes
	
			// and the array length is: d.
} Sig_MT;

typedef struct stack_node
{
	unsigned char * node; // an n-byte array

	size_t node_height;

} stack_node;

#if 0
The below define:

1. len_1

2. len_2

3. w

4. h

See Section 3.1.1

and

Section 5.3 on XMSS Parameters

in RFC 8391

for an explanation on the values of these

parameters for XMSS-SHA2_20/4_512

#endif

const size_t len_1 = 128;

const size_t len_2 = 3;

const size_t w = 16;

const size_t n = 64;

const size_t h = 20;

const size_t d = 4;

// be sure to initialize all bytes in outsize to zero initially

// Since w := 16 throughout this implementation, the final length
//
// of the base_w output array is: 2 * insize

#if 0
This function extracts the (h - h/d) most signifcant bits

from a 32-bit unsigned integer.

#endif
unsigned int msb_x(unsigned int x)
{

	unsigned int bits_delete = 0;

	const size_t bits_delete_num = sizeof(unsigned int) * 8 - (h - h / d);

	size_t i = 0;

	while ( i < bits_delete_num )
	{
		bits_delete |= 1;

		i++;
		
		if ( i != bits_delete_num )
		{
			bits_delete <<= 1;
		}	
	}
	
	printf("bits_delete: %x\n",bits_delete);
	
	bits_delete = ~bits_delete;

	printf("bits_delete: %x\n",bits_delete);

	return x & bits_delete;	

}

unsigned int lsb_x(unsigned int x)
{
	unsigned int bits_extract = 0;
	
	const size_t bits_delete_num = ( h / d );

	size_t i = 0;

	while ( i < bits_delete_num )
	{
		bits_extract |= 1;

		i++;

		if ( i != bits_delete_num )
		{
			bits_extract <<= 1;
		}	
	}

	return x & bits_extract; 	

}


void base_w(unsigned char * input,size_t insize,size_t w,unsigned char * output,size_t outsize)
{
	size_t in = 0, out = 0, bits = 0, consumed = 0;

	unsigned char total = 0;

	for ( consumed = 0; consumed < outsize ; consumed++ )
	{
		if ( bits == 0 )
		{
			total = input[in];

			in++;

			bits += 8;
		}

		bits -= ( (size_t)(log(w)/log(2)) );


		output[out] = ( ( total >> bits ) & (w-1) );

		out++;
	}

	return output;
}

void toByte(unsigned char * out,size_t outsize,unsigned long long input)
{
	memset(out,0x00,outsize*sizeof(unsigned char));

	for ( size_t i = outsize - 1 ; (i >= 0) && (i < outsize) ; i-- )
	{
		out[i] = ( input & 0xff );

		input >>= 8;	
	}
	
}

#if 0
n == 64 bytes

ADRS: 32-byte string

SEED: An n-byte randomized string

output: An n-byte output string

PRF: SHA2-512(toByte(3,64) || (KEY := SEED) || M)


F: SHA2-512(toByte(0,64) || KEY || M)
#endif

// PRF: SHA2-512(toByte(3,64) || (key := SEED) || M)

void prf(unsigned char * KEY,size_t KEYSIZE,unsigned char * seed,unsigned char * m)
{

#if 0

\'KEY\' and \'key\' are not the same thing!

\'KEY\' is the output of prf whereas \'key\' is the seed as the key input for

prf ( See RFC 8391, Section 3.1.1.1. WOTS+ Functions )

Since toByte gives a 64-byte output, and key is 64 bytes, and

M is m, the size of the input to SHA2-512 is 64+64+32(sizeof(m))

==  160 bytes

But the final output of prf is n: which has been chosen in this implementation

to be 64 bytes
#endif
	size_t ivsize = 64, seedsize = 64, msize = 32, sha512_input_size = 160;
	
	unsigned char iv[ivsize], sha512_input[160];

	unsigned long long toByte_input = 3;

	memset(iv,0x00,ivsize*sizeof(unsigned char));

	memset(sha512_input,0x00,sha512_input_size*sizeof(unsigned char));

	toByte(iv,ivsize*sizeof(unsigned char),toByte_input);

	size_t sha512_inputi = 0, seedi = 0, mi = 0;

	while ( i < ivsize )
	{
		sha512_input[sha512_inputi] = iv[sha512_inputi];

		sha512_inputi++;
	}	

	while ( seedi < seedsize )
	{
		sha512_input[sha512_inputi] = seed[seedi];

		sha512_inputi++;

		seedi++;
	}

	sha512_inputi = sha512_input_size - 1;

	while ( mi < msize )
	{
		sha512_input[sha512_inputi] = m[mi];
		
		mi++;

		sha512_inputi++;
	}

	// The above completes the formation of sha512_input[]
	
	SHA512(sha512_input,sha512_input_size,KEY);	
		
}


// F: SHA2-512(toByte(0,64) || KEY || M)

void f(unsigned char * KEY,size_t KEYSIZE,unsigned char * seed,unsigned char * adrs)
{

#if 0

\'KEY\' and \'key\' are not the same thing!

\'KEY\' is the output of prf whereas \'key\' is the seed as the key input for

prf ( See RFC 8391, Section 3.1.1.1. WOTS+ Functions )

Since toByte gives a 64-byte output, and key is 64 bytes, and

M is adrs, the size of the input to SHA2-512 is 64+64+32(sizeof(adrs))

==  160 bytes

But the final output of prf is n: which has been chosen in this implementation

to be 64 bytes
#endif
	size_t ivsize = 64, seedsize = 64, msize = 32, sha512_input_size = 160;
	
	unsigned char iv[ivsize], sha512_input[160];

	unsigned long long toByte_input = 0;

	memset(iv,0x00,ivsize*sizeof(unsigned char));

	memset(sha512_input,0x00,sha512_input_size*sizeof(unsigned char));

	toByte(iv,ivsize*sizeof(unsigned char),toByte_input);

	size_t sha512_inputi = 0, seedi = 0, mi = 0;

	while ( i < ivsize )
	{
		sha512_input[sha512_inputi] = iv[sha512_inputi];

		sha512_inputi++;
	}	

	while ( seedi < seedsize )
	{
		sha512_input[sha512_inputi] = seed[seedi];

		sha512_inputi++;

		seedi++;
	}

	sha512_inputi = sha512_input_size - 1;

	while ( mi < msize )
	{
		sha512_input[sha512_inputi] = adrs[mi];
		
		mi++;

		sha512_inputi++;
	}

	// The above completes the formation of sha512_input[]
	
	SHA512(sha512_input,sha512_input,KEY);	
		
}

#if 0
H: SHA2-512(toByte(1,64) || KEY || M)

accepts n-byte KEYS
#endif

void h(unsigned char * out,size_t outsize,unsigned char * key,size_t keysize,unsigned char * msg,size_t msgsize)
{
	size_t one_to_bytestring_size = 64, sha512_input_size = n * 2 + n * 2;

	unsigned char one_to_bytestring[one_to_bytestring_size];

	unsigned char sha512input[sha512_input_size];

	memset(one_to_bytestring,0x00,one_to_bytestring_size*sizeof(unsigned char));
	
	memset(sha512input,0x00,sha512_input_size*sizeof(unsigned char));

	toByte(one_to_bytestring,one_to_bytestring_size,1);

	size_t sha512input_index = 0, one_to_bytestring_index = 0, key_index = 0, msg_index = 0;

	for ( ; one_to_bytestring_index < one_to_bytestring_size ; sha512input_index++ )
	{
		sha512input[sha512input_index] = one_to_bytestring[one_to_bytestring_index];

		sha512input_index++;

		one_to_bytestring_index++;
	}
	
	//for ( ; key_index < keysize ; out_index++, key_index++ )
	for ( ; key_index < keysize ;  )
	{
		sha512input[sha512input_index] = key[key_index];

		sha512input_index++;

		key_index++;
	}

	//for ( ; msg_index < msgsize ; out_index++, msg_index++ )
	for ( ; msg_index < msgsize ;  )
	{
		sha512input[sha512input_index] = msg[msg_index];

		sha512input_index++;

		msg_index++;
	}
	
	SHA512(sha512_input,sha512_input_size,out);	

}

#if 0
H_msg: SHA2-512(toByte(2,64) || KEY || M)

msg := M

msgsize: size 64 bytes == 512 bits

accepts 3n-byte KEYs

sha512_input_size := 64 toByte(2,64) + 64 * 3 (sizeof(KEY)) + 64 (sizeof(M))
#endif

void h_msg(unsigned char * out,size_t outsize,unsigned char * key,size_t keysize,unsigned char * msg,size_t msgsize)
{
	size_t two_to_bytestring_size = 64, sha512_input_size = n * 5;

	unsigned char two_to_bytestring[two_to_bytestring_size];

	unsigned char sha512input[sha512_input_size];

	memset(two_to_bytestring,0x00,two_to_bytestring_size*sizeof(unsigned char));
	
	memset(sha512input,0x00,sha512_input_size*sizeof(unsigned char));

	toByte(two_to_bytestring,two_to_bytestring_size,2);

	size_t sha512input_index = 0, two_to_bytestring_index = 0, key_index = 0, msg_index = 0;

	for ( ; two_to_bytestring_index < two_to_bytestring_size ; )
	{
		sha512input[sha512input_index] = two_to_bytestring[two_to_bytestring_index];

		sha512input_index++;

		two_to_bytestring_index++;
	}
	
	for ( ; key_index < keysize ;  )
	{
		sha512input[sha512input_index] = key[key_index];

		sha512input_index++;

		key_index++;
	}

	for ( ; msg_index < msgsize ;  )
	{
		sha512input[sha512input_index] = msg[msg_index];

		sha512input_index++;

		msg_index++;
	}
	
	SHA512(sha512_input,sha512_input_size,out);	

}

void build_auth	(
			unsigned char auth[][]
			,

			SK * xmss_sk
			
			,
			
			size_t i
			
			,
			
			adrs * address
		)
{
	size_t k = 0;

	for ( size_t j = 0 ; j < h ; j++ )
	{
		k = (i / ((size_t)(pow(2,j)))) ^ 1;
		treehash(
				auth[j],
				
				xmss_sk,
				
				k * (size_t)pow(2,j)
				,

				j

				,

				address
			);
	}	
}

unsigned char * getWOTS_SK(SK * xmss_sk,size_t i)
{
	return xmss_sk->pk[i];
}

#if 0
 set_hash_address: sets the hash address in the OTS hash address scheme:

 (See RFC 8391: Section 2.5).

 This is the second to last 32 bits in the OTS hash address scheme	

 Algorithm 2: WOTS+ Private Key

 See Section 3.1.3
#endif

unsigned char * chain(unsigned char * input,size_t insize,size_t n,uint32_t i,uint32_t s,const unsigned char seed[],adrs * address)
{
	if ( s == 0 )
	{
		return input;
	}

	if ( ( i + s )  > ( w - 1 ) )
	{
		return NULL;
	}

	unsigned char tmp[n];

	memset(tmp,0x00,n*sizeof(unsigned char));

	tmp = chain(input,insize,n,i,s-1,seed,address);

	address->hash_address = i + s - 1;

	address->key_and_mask = 0x00;

	unsigned char key[65];

	memset(key,0x00,65*sizeof(unsigned char));

	const size_t keysize = 64;

	prf(key,keysize,seed,address);

	unsigned char bm[65];

	memset(bm,0x00,65*sizeof(unsigned char));

	prf(bm,keysize,seed,address);

	size_t i = 0;
	
	while ( i < keysize )
	{
		tmp[i] = tmp[i] ^ bm[i];

		i++;
	}

	memset(tmp,0x00,n*sizeof(unsigned char));

	tmp = F(key,tmp);

	return tmp;
}

#if 0
Algorithm 3: WOTS+ Private Key

Found in Section 3.1.3 in RFC 8391
#endif

void wots_gensk(unsigned char ** sk)
{
// The below code uses the CSPRNG from the
// OpenSSL Library
	
	for ( size_t i = 0; i < len ; i++ )
	{
		RAND_priv_bytes(sk[i],n*sizeof(unsigned char));	
	}
}

#if 0
Algorithm 4: WOTS_genPK

See Section 3.1.4
#endif
void wots_genpk(
			unsigned char pk[][]
			
			,
			
			unsigned char wots_sk[][]
			,

			adrs * address
			
			,

			unsigned char seed[]
			
		)
{
	for ( size_t i = 0; i < len ; i++ )
	{
		address.chain_address = i;

		pk[i] = chain(sk[i],n,n,0,w-1,seed,address);
	}
}

#if 0
Algorithm 5: from RFC 8391

WOTS_sign:

M: a message that must be a string of base-16

numbers to conform to base_w's requirements.

(See Section 3.1.1)

This algorithm requires calculation of WOTS+ parameters

len_1 and len_2 as defined in Section 3.1.1 of RFC 8391

Since n := 64 bytes and w := 16:

len_1 = 128

len_2 = 3 throughout this implementation

size(M) == 64 bytes. It should be a 64-byte

output of a cryptographically secure message

digest such as SHA2-512.
#endif

void wots_sign	(
			unsigned char wots_sig[][]
			,
			
			unsigned char ** sk
			
			,

			unsigned char M[]
			,

			size_t msize
			
			,

			
			adrs * address
			
			,
			
			unsigned char seed[]
		)
{

#if 0
   There is an errata in RFC 8391.

   The document erroneously says the max
   
   value of csum is:

   len_1 * (w-1) * 2^8

   According to the Errata section, the

   correct maximum possible value of csum is:

   len_1 * (w-1)
#endif
	uint32_t csum = 0;

	size_t msgsize = len_1 + len_2;
	
	unsigned char msg[msgsize];

	memset(m,0x00,msgsize*sizeof(unsigned char));

	base_w(M,msize,w,msg,msgsize);

	for ( size_t i = 0; i < len_1 ; i++ )
	{
		csum = csum + w - 1 - msg[i];
	}

	csum = csum << ( 8 - ( ( len_2 * (size_t)(log(w)/log(2)) ) % 8 ) );

	size_t len_2_bytes = ceil( ( len_2 * (size_t)( log(w)/log(2) ) ) / 8 );

	size_t csum_to_bytes_size = len_2_bytes;

	unsigned char csum_to_bytes[csum_to_bytes_size];

	memset(csum_to_bytes,0x00,csum_to_bytes_size*sizeof(unsigned char));

	toByte(csum_to_bytes,csum_to_bytes_size,csum);


	size_t csum_base_w_size = len_2;

	unsigned char csum_base_w[csum_base_w_size];

	memset(csum_base_w,0x00,csum_base_w_size*sizeof(unsigned char));	
	
	base_w(csum_to_bytes,csum_to_bytes_size,w,csum_base_w,csum_base_w_size);

	for	(
			size_t i = csum_base_2_size - 1, j = msgsize - 1
		
			; 
		
			( i >= 0 ) && ( i < csum_base_w_size ) 
			; 
		
			i--, j--
		
		)
	{
		msg[j] = csum_base_w[i];
	}

	for	(
			size_t i = 0

			;

			i <  len

			;

			i++
		)
	{
	
		address->chain_address = i;

		wots_sig[i] = chain(sk[i],0,msg[i],seed,address);
			
	}
}

#if 0
Algorithm 6: WOTS_pkFromSig - Computing a WOTS+ public key from a message and its signature

Its template is just like Algorithm 5, so

Algorithm 5 is copied here.

There is an errata in RFC 8391.

The document erroneously says the max

value of csum is:

len_1 * (w-1) * 2^8

According to the Errata section, the

correct maximum possible value of csum is:

len_1 * (w-1)
#endif

void wots_pk_from_sign	(
				unsigned char tmp_pk[][]	
				
				,
				unsigned char wots_sig[][]
				
				,	
				
				unsigned char M[]
				
				,

				size_t msize

				,

				
				adrs * address
				
				,
				
				unsigned char seed[]
			)
{

#if 0
   There is an errata in RFC 8391.

   The document erroneously says the max
   
   value of csum is:

   len_1 * (w-1) * 2^8

   According to the Errata section, the

   correct maximum possible value of csum is:

   len_1 * (w-1)
#endif
	uint32_t csum = 0;

	size_t msgsize = len_1 + len_2;
	
	unsigned char msg[msgsize];

	memset(m,0x00,msgsize*sizeof(unsigned char));

	base_w(M,msize,w,msg,msgsize);

	for ( size_t i = 0; i < len_1 ; i++ )
	{
		csum = csum + w - 1 - msg[i];
	}

	csum = csum << ( 8 - ( ( len_2 * (size_t)(log(w)/log(2)) ) % 8 ) );

	size_t len_2_bytes = ceil( ( len_2 * (size_t)( log(w)/log(2) ) ) / 8 );

	size_t csum_to_bytes_size = len_2_bytes;

	unsigned char csum_to_bytes[csum_to_bytes_size];

	memset(csum_to_bytes,0x00,csum_to_bytes_size*sizeof(unsigned char));

	toByte(csum_to_bytes,csum_to_bytes_size,csum);


	size_t csum_base_w_size = len_2;

	unsigned char csum_base_w[csum_base_w_size];

	memset(csum_base_w,0x00,csum_base_w_size*sizeof(unsigned char));	
	
	base_w(csum_to_bytes,csum_to_bytes_size,w,csum_base_w,csum_base_w_size);

	for	(
			size_t i = csum_base_w_size - 1, j = msgsize - 1
		
			; 
		
			( i >= 0 ) && ( i < csum_base_w_size ) 
			; 
		
			i--, j--
		
		)
	{
		msg[j] = csum_base_w[i];
	}

	for	(
			size_t i = 0

			;

			i <  len

			;

			i++
		)
	{
	
		address->chain_address = i;

		tmp_pk[i] = chain(wots_sig[i],msg[i],w-1-msg[i],seed,address);
			
	}
}

#if 0
Algorithm 7: RAND_HASH
#endif

void rand_hash	(
			unsigned char rand_hash_output[]
			
			,

			size_t rand_hashsize

			,

			unsigned char left[]

			,

			unsigned char right[]

			,

			unsigned char seed[]

			,

			adrs * address

		)
{
	address->key_and_mask = 0;

	unsigned char key[n];

	memset(key,0x00,n*sizeof(unsigned char));
	
	prf(key,n,seed,address);

	address->key_and_mask = 1;

	unsigned char bm_0[n], bm_1[n];

	memset(bm_0,0x00,n*sizeof(unsigned char));
	
	memset(bm_1,0x00,n*sizeof(unsigned char));

	prf(bm_0,n,seed,address);

	prf(bm_1,n,seed,address);

	unsigned char left_xor_bm_0[n], right_xor_bm_1[n];

	memset(left_xor_bm0,0x00,n*sizeof(unsigned char));

	memset(right_xor_bm1,0x00,n*sizeof(unsigned char));

	for ( size_t i = 0 ; i < n ; i++ )
	{
		left_xor_bm_0[i] = left[i] ^ bm_0[i];

		right_xor_bm_1[i] = right[i] ^ bm_1[i];
	}

	size_t msgsize = n * 2, msg_index = 0;

	size_t left_xor_bm_0_index = 0, right_xor_bm_1_index = 0;

	unsigned char msg[msgsize];

	for (  ; left_xor_bm_0_index < n ; msg_index++, left_xor_bm_0_index++ )
	{
		msg[msg_index] = left_xor_bm_0[left_xor_bm_0_index];
	}

	for (  ; right_xor_bm_1_index < n ; msg_index++, right_xor_bm_1_index++ )
	{
		msg[msg_index] = right_xor_bm_1[right_xor_bm_1_index];
	}

	size_t h_outsize = n * 2 + n * 2;

	unsigned char h_output[h_outsize];

	memset(h_output,0x00,h_outsize * sizeof(unsigned char));

	h(rand_hash_output,rand_hashsize,key,keysize,msg,msgsize);	
}

#if 0
Algorithm 8: ltree

See Section 4.1.5
#endif

void ltree	(

			unsigned char pk_zero[]

			,
			
			unsigned char wots_pk[][]

			,

			adrs * address

			,

			unsigned char seed[]

		)
{
	address->tree_height = 0;

	size_t len_clone = len;

	while ( len_clone )
	{
		for ( size_t i = 0; i < len_clone / 2 ; i++ )
		{
			address.tree_index = i;
			
			rand_hash(wots_pk[i],wots_pk[2 * i],wots_pk[2 * i + 1],seed,address);

					
		}

		if ( len_clone % 2 == 1 )
		{
			wots_pk[len_clone / 2] = wots_pk[len_clone - 1];
		
		}

		len_clone = (size_t)ceil( len_clone / 2.0);
		
		address.tree_height = address.tree_height + 1;
	
	}
	
	memcpy(pk_zero,wots_pk[0],n*sizeof(unsigned char));
}

#if 0
Algorithm 9: treeHash
#endif

void treehash(unsigned char * root_node,SK * xmss_sk,size_t s,size_t t,adrs * address)
{
	unsigned char node[n];

	unsigned char * seed = 0x00;

	unsigned char pk[len][n];

	stack_node stack[t-1];

	memset(stack,0x00,(t-1)*sizeof(stack_node));

	stack_node * sp = &stack[0] - 1;

	for ( size_t i = 0 ; i < len ; i++ )
	{
		memset(pk[i],0x00,n*sizeof(unsigned char));
	}

	if ( s % (1 << t) != 0 )
	{
		return -1;
	}

	for ( size_t i = 0; i < (size_t)(pow(2,t)) ; i++ )
	{
		seed = xmss_sk->seed;

		address->type = 0;

		address->ots_address = s + i;
		
		wots_genpk(pk,(xmss_sk->wots_sk)[s+i],seed,address);

		address->type = 1;

		address->ltree_address = s + i;
		
		ltree(node,pk,seed,address);
		
		address->type = 2;

		address->tree_height = 0;

		address->tree_index = i + s;

		while (
				( sp >= &sp[0] ) 		
	
				&&

				( sp->node_height == address->tree_height )
		      )
		{
			address->tree_index = ( ( address->tree_index - 1 ) / 2);

			rand_hash(node,sp->node,node,seed,address);

			memset(sp->node,0x00,n*sizeof(unsigned char));

			sp->node_height = 0;

			sp--;	

			address->tree_height = address->tree_height + 1;
		
		}
		
		sp++;

		memcpy(sp->node,node,n*sizeof(unsigned char));

		sp->node_height = address->tree_height;	
	
	}

	memcpy(root_node,sp->node,n*sizeof(unsigned char));
	
	// This algorithm can literally fail and return sp[-1]
	//
	// according to Errata	
}

#if 0
Algorithm 10: XMSS_keygen

The XMSS Parameter "h" has been chosen

to be: 20 as this file implements XMSS-SHA2_20_512.

For an explanation of this paramater choice,

see Section 5.3 "XMSS Paramaters" in RFC 8391
#endif

void xmss_keygen(
			SK * xmss_sk
			
			,

			PK * pk

			,

			unsigned char wots_sk[][]
		 ,
		 	unsigned char xmss_sk[]
		 ,

		 	unsigned char xmss_pk[]	
		 
		 )

{
	size_t idx = 0;

	for ( size_t i = 0; i < (size_t)pow(2,h) ; i++ )
	{
		wots_gensk((xmss_sk->wots_sk)[i]);
	}

	RAND_priv_bytes(xmss_sk->sk_prf,n*sizeof(unsigned char));

	RAND_priv_bytes(xmss_sk->seed,n*sizeof(unsigned char));

	adrs address;

	memset(address,0x00,sizeof(adrs));

	xmss_sk->idx = idx;

	treehash(xmss_sk,0,h,address);
}	

#if 0
Algorithm 11: treeSig

See Section 4.1.9 in RFC 8391
#endif
void treesig(
		Sig * sig
		
		,

		unsigned char m[]

		,

		size_t msize

		,

		SK * xmss_sk

		,

		size_t idx_sig

		,

		adrs * address
	    )
{
	build_auth(sig->auth,xmss_sk,idx_sig,address);

	address->type = 0;

	address->ots_address = idx_sig;

	wots_sign(sig->sig_ots,(xmss_sk->wots_sk)[idx_sig],m,msize,xmss_sk->seed,address);

}

#if 0
Algorithm 12: XMSS_sign

See Section 4.1.9
#endif

void xmss_sign	(
			SK * xmss_sk

			,

			Sig * sig

			,

			unsigned char m[]

			,

			size_t msize

		)
{
	(xmss_sk->idx)++;

	adrs address;

	memset(address,0x00,sizeof(adrs));

	size_t idx_sig_tobyte_size = n / 2, mprime_size = n;

	unsigned char r[n], root_node[n], mprime[n], idx_sig_tobyte[idx_sig_tobyte_size], key[3*n];

	memset(r,0x00,n*sizeof(unsigned char));
	
	memset(root_node,0x00,n*sizeof(unsigned char));
	
	memset(key,0x00,3*n*sizeof(unsigned char));

	memset(mprime,mprime_size*sizeof(unsigned char));

	memset(idx_sig_tobyte,0x00,idx_sig_tobyte_size*sizeof(unsigned char));

	toByte(idx_sig_tobyte,idx_sig_tobyte_size,idx);

	prf(r,n*sizeof(unsigned char),xmss_sk->sk_prf,idx_sig_tobyte);
	
	memcpy(key,r,n*sizeof(unsigned char));

	memcpy(key,root_node,n*sizeof(unsigned char));

	memcpy(key,idx_sig_tobyte,idx_sig_tobyte_size*sizeof(unsigned char));

	h_msg(mprime,n*sizeof(unsigned char),key,keysize,m,msize);

	sig->idx_sig = xmss_sk->idx;

	memcpy(sig->r,r,n*sizeof(unsigned char));

	treesig(sig,mprime,m,mprime_size,xmss_sk,xmss_sk->idx,address);
}

#if 0
Algorithm 13:

See Section 4.1.10

xmss_rootfromsig - Compute a root node from a tree signature
#endif

void xmss_root_from_sign	(
					unsigned char * root_node_zero
					
					,

					size_t idx_sig
					
					,

					unsigned char ** sig_ots

					,

					unsigned char ** auth

					,

					unsigned char mprime[]

					,

					size_t mprime_size

					,

					unsigned char * seed

					,

					adrs * address

				)
{
	address->type = 0;

	address->ots_address = idx_sig;

	unsigned char pk_ots[len][n];
	
	for ( size_t r = 0; r < len ; r++ )
	{
		memset(pk_ots[r],0x00,n*sizeof(unsigned char));	
	}
	
	wots_pk_from_sign(pk_ots,sig_ots,mprime,mprime_size,address,seed);

	address->type = 1;

	address->ltree_address = idx_sig;

	unsigned char node[2][n];

	for ( size_t r = 0 ; r < 2 ; r++ )
	{
		memset(node[r],0x00,n*sizeof(unsigned char));
	}

	// forgot to initialize node[0] here
	
	ltree(node[0],pk_ots,seed,address);

	address->type = 2;

	address->tree_index = idx_sig;

	for ( size_t k = 0 ; k < h ; k++ )
	{
		address->tree_height = k;

		if ( ( idx_sig / (size_t)pow(2,k) ) % 2 == 0 )
		{
			address->tree_index = ( address->tree_index / 2 );
		}

		rand_hash(node[1],n*sizeof(unsigned char),auth[k],node[0],seed,address);
	}

	memcpy(root_node_zero,node[1],n*sizeof(unsigned char));
}

#if 0
Algorithm 14: XMSS_verify

See Section 4.1.10 in RFC 8391
#endif

bool xmss_verify	(
				Sig * sig

				,

				unsigned char M[]

				,

				size_t msize

				,

				PK * pk	

			)
{
	adrs address;

	memset(address,0x00,sizeof(adrs));

	size_t mprime_size = 0, idx_sig_tobyte_size = n*sizeof(unsigned char);

	size_t keysize = 3 * n * sizeof(unsigned char);

	unsigned char mprime[n];

	unsigned char idx_sig_tobyte[n*sizeof(unsigned char))];

	memset(idx_sig_tobyte,0x00,n*sizeof(unsigned char));

	toByte(idx_sig_tobyte,idx_sig_tobyte_size,sig->idx_sig);

	unsigned char KEY[3*n*sizeof(unsigned char)];

	memset(KEY,0x00,3*n*sizeof(unsigned char));

	memcpy(KEY,sig->r,n*sizeof(unsigned char));

	memcpy(KEY,pk->root_node,n*sizeof(unsigned char));

	memcpy(KEY,idx_sig_tobyte,n*sizeof(unsigned char));

	memset(mprime,0x00,n*sizeof(unsigned char));	

	h_msg(mprime,mprime_size,KEY,keysize,M,msize);

	unsigned char node[n*sizeof(unsigned char)];

	memset(node,0x00,n*sizeof(unsigned char));

	xmss_root_node_from_sign(node,sig->idx_sig,sig->sig_ots,sig->auth,mprime,pk->seed,address);

	for ( size_t i = 0 ; i < n ; i++ )
	{
		if ( node[i] != pk->root_node[i] )
		{
			return false;
		}
	}

	return true;
}

#if 0
Algorithm 15: XMSSMT_keyGEN

Section 4.2.2
#endif

void xmssmt_keygen	(
				SK_MT * xmss_sk_mt

				,

				PK_MT * xmss_pk_mt	
			)
{	
	unsigned char wots_sk[len][n];

	for ( size_t r = 0 ; r < len ; r++ )
	{
		memset(wots_sk,0x00,n*sizeof(unsigned char));	
	}

	xmss_sk_mt->idx_mt = 0;

	RAND_priv_bytes(xmss_sk_mt->sk_prf,n*sizeof(unsigned char));

	RAND_priv_bytes(xmss_sk_mt->seed,n*sizeof(unsigned char));

	adrs address;

	memset(address,0x00,sizeof(adrs));

	for ( size_t layer = 0 ; layer < d ; layer++ )
	{
		address.layer_address = (uint32_t)layer;

		for ( size_t tree = 0 ; tree < ( 1 << ( ( d - 1 - layer ) * (h / d ) ) ) ; tree++ )
		{
			address.tree_address = (uint32_t)tree;

			for ( size_t i = 0; i < (size_t)pow(2,h/d) ; i++ )
			{
				wots_gensk(wots_sk[i]);
			}

			for ( size_t i = 0; i < len ; i++ )
			{
				// consider whether or not you must change
				// the dimensions to [layer][tree]

				((xmss_sk_mt->sk_map)[layer][tree])->wots_sk

				// initialize the wots_sk 3D array here
				
			}

		}
	}

	SK * sk = xmss_sk_mt->sk_map[d-1][0];

	unsigned char seed[n*sizeof(unsigned char)];

	RAND_priv_bytes(seed,n*sizeof(unsigned char));

	memcpy(xmss_sk_mt->seed,seed,n*sizeof(unsigned char));

	unsigned char root[n*sizeof(unsigned char)];

	memset(root,0x00,n*sizeof(unsigned char));

	treehash(root,sk,0,h/d,address);

	memcpy(xmss_sk_mt->root,root,n*sizeof(unsigned char));

	memcpy(pk_mt->algorithm,"OID\0",4*sizeof(unsigned char));

	memcpy(pk_mt->root,root,n*sizeof(unsigned char));

	memcpy(pk_mt->seed,seed,n*sizeof(unsigned char));

}

#if 0
Algorithm 16: XMSSMT_sign: Generate an XMSS^MT Signature and Update

the XMSS^MT private key

Section 4.2.4
#endif

void xmssmt_sign	(
				Sig_MT * xmss_sig_mt

				,


				unsigned char m[]

				,

				size_t msize

				,

				SK_MT * xmss_sk_mt

			)
{
	adrs address;

	memset(address,0x00,sizeof(adrs));

	unsigned char seed[n*sizeof(unsigned char)];

	
	memset(seed,0x00,n*sizeof(unsigned char));

	unsigned char sk_prf[n*sizeof(unsigned char)];
	
	memset(sk_prf,0x00,n*sizeof(unsigned char));

	memcpy(sk_prf,xmss_sk_mt->sk_prf,n*sizeof(unsigned char));

	size_t idx_sig = xmss_sk_mt->idx_mt;

	(xmss_sk_mt->idx_mt)++;

	unsigned char r[n * sizeof(unsigned char)];

	memset(r,0x00,n*sizeof(unsigned char));

	unsigned char mprime[n*sizeof(unsigned char)];

	memset(mprime,0x00,n*sizeof(unsigned char));

	size_t idx_sig_tobyte_size = 32 * sizeof(unsigned char);
	
	unisgned char idx_sig_tobyte[idx_sig_tobyte_size];

	memset(idx_sig_tobyte,0x00,idx_sig_tobyte_size*sizeof(unsigned char));
	
	toByte(idx_sig_tobyte,idx_sig_tobyte_size*sizeof(unsigned char),idx_sig);

	prf(r,sk_prf,idx_sig_tobyte);

	unsigned char mprime[n*sizeof(unsigned char)];

	memset(mprime,0x00,n*sizeof(unsigned char));

	size_t h_msg_keysize = 3 * n * sizeof(unsigned char);

	unsigned char h_msg_key[3*n*sizeof(unsigned char)];

	memset(h_msg_key,0x00,h_msg_keysize);

	memcpy(h_msg_key,r,h_msg_keysize);

	memcpy(h_msg_key,xmss_sk_mt->root,n*sizeof(unsigned char));
	
	size_t idx_sig_to_byten_size = n*sizeof(unsigned char);

	unsigned char idx_sig_to_byten[idx_sig_to_byten_size];

	memset(idx_sig_to_byten,0x00,idx_sig_to_byten_size*sizeof(unsigned char));

	toByte(idx_sig_to_byten,idx_sig_to_byten_size,idx_sig);

	memcpy(h_msg_key,idx_sig_to_byten,idx_sig_to_byten_size*sizeof(unsigned char));

	h_msg(mprime,n*sizeof(unsigned char),h_msg_key,h_msg_keysize,m,msize);

	xmss_sig_mt->idx_sig = idx_sig;

	unsigned int idx_tree = msb_x(idx_sig); // ( h - h / d ) most significant bits of idx_sig
				// that's ( 20 - 20 / 4 ) == 15 most significant bits
	
	unsigned int idx_leaf = lsb_x(idx_sig);
	
	SK * sk = xmss_sk_mt->sk_map[0][idx_tree];

	sk->idx = idx_leaf;

	memcpy(sk->sk_prf,sk_prf,n*sizeof(unsigned char));

	memcpy(sk->seed,xmss_sk_mt->seed,n*sizeof(unsigned char));

	memcat(sk->root,0x00,n*sizeof(unsigned char));	

	address->layer_address = 0;

	address->tree_address = idx_tree;

	Sig sig_tmp;

	treeSig(&sig_tmp,mprime,sk,idx_leaf,address);

	memcpy(xmss_sig_mt->r,r,n*sizeof(unsigned char));

	memcpy((xmss_sig_mt->sig_arr)[address->layer_address],sig_tmp,sizeof(Sig));
	unsigned char root_node[n*sizeof(unsigned char)];

	memset(root_node,0x00,n*sizeof(unsigned char));
	
	for ( size_t j = 1 ; j < d ; j++ )
	{
		treehash(root_node,sk,0,h/d,address);					
		idx_leaf = lsb_x(idx_tree,h/d);

		idx_tree = msb_x(idx_tree,h - j * ( h / d ) );

		sk = (xmss_sk_mt->sk_map)[j][idx_tree];

		memcpy(sk->sk_prf,sk_prf,n*sizeof(unsigned char));

		memcat(sk->root,0x00,n*sizeof(unsigned char));

		memcpy(sk->seed,seed,n*sizeof(unsigned char));

		address->layer_address = j;

		address->tree_address = idx_tree;

		treeSig(&sig_tmp,root,sk,idx_leaf,address);

		memcpy((xmss_sig_mt->sig_arr)[address->layer_address],sig_tmp,n*sizeof(Sig));

	}
}

#if 0
Algorithm 17

See Section 4.2.5 in RFC 8391
#endif

bool xmsst_verify	(
				Sig_MT * xmss_sig_mt

				,

				unsigned char m[]

				,

				size_t msize

				,

				PK_MT * xmss_pk_mt
			)
{
	size_t idx_sig = xmss_sig_mt->idx_sig;

	unsigned char seed[n*sizeof(unsigned char)];

	memcpy(seed,xmss_pk_mt->seed,n*sizeof(unsigned char));

	adrs address;

	memset(address,0x00,sizeof(adrs));

	unsigned char mprime[n*sizeof(unsigned char)];

	size_t keysize = 3 * n * sizeof(unsigned char);

	unsigned char key[keysize];

	memset(key,0x00,3*n*sizeof(unsigned char));

	memcpy(key,xmss_sig_mt->r,n*sizeof(unsigned char));

	memcpy(key,xmss_pk_mt->root,n*sizeof(unsigned char));

	size_t idx_sig_tobyte_size = 32;

	unsigned char idx_sig_tobyte[idx_sig_tobyte_size];

	memset(idx_sig_tobyte,0x00,idx_sig_tobyte_size*sizeof(unsigned char));

	toByte(idx_sig_tobyte,idx_sig_tobyte_size*sizeof(unsigned char),0);

	memcpy(key,idx_sig_tobyte,idx_sig_tobyte_size*sizeof(unsigned char));

	h_msg(mprime,n*sizeof(unsigned char),key,keysize,m,msize);

	unsigned int idx_leaf = lsb_x(idx_sig);

	unsigned int idx_tree = msb_x(idx_sig);

	Sig * sigprime = (xmss_sig_mt->sig_arr)[0];

	address.layer_address = 0;

	address.tree_address = idx_tree;

	unsigned char node[n*sizeof(unsigned char)];

	memset(node,0x00,n*sizeof(unsigned char));

	xmss_root_from_sign(node,idx_leaf,sigprime->sig_ots,sigprime->auth,mprime,n*sizeof(unsigned char),seed,address);

	for ( size_t j = 1 ; j < d ; j++ )
	{
		idx_leaf = lsb_x(idx_tree);

		idx_tree = msb_x(idx_tree);

		sigprime = (xmss_sig_mt->sig_arr)[j];

		address.layer_address = j;

		address.tree_address = idx_tree;

		// Fix the below line. Its not correct:

		xmss_root_from_sign(node,idx_leaf,sigprime->sig_ots,sigprime->auth,mprime,n*sizeof(unsigned char),seed,address);

	}

	for ( size_t i = 0; i < n ; i++ )
	{
		if ( node[i] != (xmss_pk_mt->root)[i] )
		{
			return false;
		}
	}

	return true;
}
